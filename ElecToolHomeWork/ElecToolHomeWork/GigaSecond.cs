﻿namespace ElecToolHomeWork
{
    public class GigaSecond
    {
        static readonly int GigaSec = 1000000000;
        static readonly int Minute = 60;
        static readonly int Hour = 60 * Minute;
        static readonly int Day = 24 * Hour;
        static readonly int DayModulo = GigaSec % Day;
        static readonly int HourModulo = DayModulo % Hour;

        
        public static string GigaSecondTransform()
        {
            int days = GigaSec / Day;
            int hours = DayModulo / Hour;
            int minutes = HourModulo / Minute;
            int seconds = HourModulo % Minute;

            return days.ToString() + ":" + hours.ToString() + ":" + minutes.ToString() + ":" + seconds.ToString();
        }
    }
}
