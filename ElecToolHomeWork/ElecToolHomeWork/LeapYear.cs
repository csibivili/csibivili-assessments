﻿namespace ElecToolHomeWork
{
    public class LeapYear
    {
        public static bool IfLeapYear(int year)
        {
            return EvenlyDivisibleByFour(year) && NotEvenlyDivisibleByHundred(year) || EvenlyDivisibleByFourHundred(year);
        }

        private static bool EvenlyDivisibleByFour(int year)
        {
            return year % 4 == 0;
        }

        private static bool NotEvenlyDivisibleByHundred(int year)
        {
            return year % 100 != 0;
        }

        private static bool EvenlyDivisibleByFourHundred(int year)
        {
            return year % 400 == 0;
        }
    }
}
