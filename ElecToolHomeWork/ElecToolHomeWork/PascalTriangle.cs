﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElecToolHomeWork
{
    public class PascalTriangle
    {
        public int[,] CreateMatrix(int input)
        {
            if (input >= 0)
            {
                int numberOfRows = input;
                int numberOfColumns = input; //* 2 - 1;

                return new int[numberOfRows, numberOfColumns];
            }
            else
            {
                return new int[0, 0];
            }
        }

        public int[,] CreateTriangle(int input)
        {
            int[,] matrix = CreateMatrix(input);

            for (int row = 0; row < input; row++)
            {
                for (int i = 0; i <= row; i++)
                {
                    if (row == i || i == 0)
                    {
                        matrix[row, i] = 1;
                    }
                    else
                    {
                        matrix[row, i] = matrix[row - 1, i - 1] + matrix[row - 1, i];
                    }
                }
            }

            return matrix;
        }
    }
}
