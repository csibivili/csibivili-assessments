using ElecToolHomeWork;
using Xunit;

namespace ElecToolHomeWorkTest
{
    public class LeapYearTest
    {
        [Fact]
        public void WhenInput1997ReturnFalse()
        {
            bool result = LeapYear.IfLeapYear(1997);

            Assert.False(result);
        }

        [Fact]
        public void WhenInput1996ReturnTrue()
        {
            bool result = LeapYear.IfLeapYear(1996);

            Assert.True(result);
        }

        [Fact]
        public void WhenInput1900ReturnFalse()
        {
            bool result = LeapYear.IfLeapYear(1900);

            Assert.False(result);
        }

        [Fact]
        public void WhenInput2000ReturnTrue()
        {
            bool result = LeapYear.IfLeapYear(2000);

            Assert.True(result);
        }
    }
}
