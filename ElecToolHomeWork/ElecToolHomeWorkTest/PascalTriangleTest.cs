using ElecToolHomeWork;
using Xunit;

namespace ElecToolHomeWorkTest
{
    public class PascalTriangleTest
    {
        [Fact]
        public void WhenInput3GetMatrix2x5()
        {
            var pascalTriangle = new PascalTriangle();

            int[,] matrix = pascalTriangle.CreateMatrix(3);

            Assert.Equal(3, matrix.GetLength(0));
            Assert.Equal(3, matrix.GetLength(1));
        }

        [Fact]
        public void WhenInput3GetFilledMatrix2x5()
        {
            var pascalTriangle = new PascalTriangle();

            int[,] matrix = pascalTriangle.CreateTriangle(3);

            Assert.Equal(1, matrix[0,0]);
            Assert.Equal(1, matrix[1,0]);
            Assert.Equal(2, matrix[2, 1]);
        }
    }
}
