using ElecToolHomeWork;
using Xunit;

namespace ElecToolHomeWorkTest
{
    public class GigaSecondTest
    {
        [Fact]
        public void GigaSecondInOtherFormat()
        {
            string gigaSecondInOtherFormat = GigaSecond.GigaSecondTransform();

            Assert.Equal("11574:1:46:40", gigaSecondInOtherFormat);
        }
    }
}
